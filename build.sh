#!/bin/bash
set -e;

WORKDIR=`pwd`;

cd $WORKDIR/dictionary;
mvn clean package;

cd $WORKDIR/english-caption-view;
mvn clean package;

cd $WORKDIR/caption-details-view;
mvn clean package;

cd $WORKDIR/eureka-server;
mvn clean package;

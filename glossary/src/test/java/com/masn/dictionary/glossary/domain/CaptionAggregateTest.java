package com.masn.dictionary.glossary.domain;

import com.masn.dictionary.glossary.caption.domain.CaptionAggregate;
import com.masn.dictionary.glossary.caption.domain.commands.ChangeCaptionKeyCommand;
import com.masn.dictionary.glossary.caption.domain.commands.ChangeEnglishCaptionValueCommand;
import com.masn.dictionary.glossary.caption.domain.commands.CreateCaptionCommand;
import com.masn.dictionary.glossary.events.CaptionEvents;
import org.axonframework.test.aggregate.AggregateTestFixture;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

class CaptionAggregateTest {

    private AggregateTestFixture<CaptionAggregate> fixture;

    @BeforeEach
    void setUp() {
        fixture = new AggregateTestFixture<>(CaptionAggregate.class);
    }

    @Test
    void testAggregateCreation() {
        UUID uuid = UUID.randomUUID();

        CaptionEvents.CaptionCreatedEvent captionCreatedEvent = CaptionEvents.CaptionCreatedEvent.newBuilder()
                .setId(uuid.toString())
                .build();

        fixture
                .when(new CreateCaptionCommand(uuid))

                .expectEvents(captionCreatedEvent)

                .expectReturnValue(uuid.toString());
    }

    @Test
    void testCaptionKeyChange() {
        UUID uuid = UUID.randomUUID();
        CaptionEvents.CaptionCreatedEvent captionCreatedEvent = CaptionEvents.CaptionCreatedEvent.newBuilder()
                .setId(uuid.toString())
                .build();

        CaptionEvents.CaptionKeyChangedEvent captionKeyChangedEvent = CaptionEvents.CaptionKeyChangedEvent.newBuilder()
                .setId(uuid.toString())
                .setKey("foo")
                .build();

        CaptionEvents.CaptionKeyChangedEvent captionKeyChangedEvent2 = CaptionEvents.CaptionKeyChangedEvent.newBuilder()
                .setId(uuid.toString())
                .setKey("bar")
                .build();

        fixture
                .given(captionCreatedEvent, captionKeyChangedEvent)

                .when(new ChangeCaptionKeyCommand(uuid, "bar"))

                .expectEvents(captionKeyChangedEvent2);
    }

    @Test
    void testEnglishValueChange() {
        UUID uuid = UUID.randomUUID();
        CaptionEvents.CaptionCreatedEvent captionCreatedEvent = CaptionEvents.CaptionCreatedEvent.newBuilder()
                .setId(uuid.toString())
                .build();

        CaptionEvents.CaptionEnglishValueChangedEvent captionEnglishValueChangedEvent = CaptionEvents.CaptionEnglishValueChangedEvent.newBuilder()
                .setId(uuid.toString())
                .setValue("foo")
                .build();

        CaptionEvents.CaptionEnglishValueChangedEvent captionEnglishValueChangedEvent2 = CaptionEvents.CaptionEnglishValueChangedEvent.newBuilder()
                .setId(uuid.toString())
                .setValue("bar")
                .build();

        fixture
                .given(captionCreatedEvent, captionEnglishValueChangedEvent)

                .when(new ChangeEnglishCaptionValueCommand(uuid, "bar"))

                .expectEvents(captionEnglishValueChangedEvent2);
    }
}
package com.masn.dictionary.glossary.caption.domain;

public enum Language {
    EN, SV, LT
}

package com.masn.dictionary.glossary.config.infra;

import com.google.protobuf.Message;

/**
 * <p>Axon event adapter interface for adapting events to Protobuf {@link com.google.protobuf.Message} instances before
 * storing events in an event store and after retrieving events from an event store.</p>
 * <p>Used when event store event schema is different than the event applied in command handlers. Or for using other
 * event classes than Protobuf {@link com.google.protobuf.Message}. Also useful for adapting Axon's
 * {@link org.axonframework.messaging.MetaData} class.</p>
 */
public interface EventAdapter {
    /**
     * Checks whether this adapter can adapt the passed type to a Protobuf {@link com.google.protobuf.Message}.
     *
     * @param type the type to adapt before storage.
     * @param <T>  the type to adapt before storage.
     * @return <tt>true</tt> when adapter is suitable, otherwise <tt>false</tt>.
     */
    <T> boolean canAdaptForStorage(Class<T> type);

    /**
     * Checks whether this adapter can adapt the passed type from a Protobuf {@link com.google.protobuf.Message}
     * to some arbitrary class.
     *
     * @param type the type to adapt from storage.
     * @param <T>  the type to adapt from storage.
     * @return <tt>true</tt> when adapter is suitable, otherwise <tt>false</tt>.
     */
    <T> boolean canAdaptFromStorage(Class<T> type);

    /**
     * Adapts the passed object to a Protobuf {@link com.google.protobuf.Message}.
     *
     * @param object the event to adapt for storage.
     * @return the adapted {@link com.google.protobuf.Message} ready for storage.
     */
    Message adaptForStorage(Object object);

    /**
     * Adapts the passed message from a Protobuf {@link com.google.protobuf.Message} to some arbitrary class.
     *
     * @param message the event to adapt after storage.
     * @return the adapted arbitrary class instance.
     */
    Object adaptFromStorage(Message message);
}

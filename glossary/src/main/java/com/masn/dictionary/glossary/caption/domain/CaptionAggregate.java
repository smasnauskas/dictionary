package com.masn.dictionary.glossary.caption.domain;

import com.masn.dictionary.glossary.caption.domain.commands.*;
import com.masn.dictionary.glossary.events.CaptionEvents;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.spring.stereotype.Aggregate;

import java.util.EnumMap;
import java.util.Map;
import java.util.Objects;

import static org.axonframework.commandhandling.model.AggregateLifecycle.apply;

@Aggregate(snapshotTriggerDefinition = "captionAggregateSnapshotTrigger")
public class CaptionAggregate {

    @AggregateIdentifier
    private String captionId;
    private String key;
    private Map<Language, String> values = new EnumMap<>(Language.class);

    public CaptionAggregate() {
    }

    @CommandHandler
    public CaptionAggregate(CreateCaptionCommand cmd) {
        apply(CaptionEvents.CaptionCreatedEvent.newBuilder()
                .setId(cmd.getUuid().toString())
                .build());
    }

    @CommandHandler
    public void handle(ChangeCaptionKeyCommand cmd) {
        if (Objects.equals(key, cmd.getKey())) {
            return;
        }

        apply(CaptionEvents.CaptionKeyChangedEvent.newBuilder()
                .setId(cmd.getUuid().toString())
                .setKey(cmd.getKey())
                .build());
    }

    @CommandHandler
    public void handle(ChangeEnglishCaptionValueCommand cmd) {
        String englishValue = values.get(Language.EN);

        if (Objects.equals(englishValue, cmd.getValue())) {
            return;
        }

        apply(CaptionEvents.CaptionEnglishValueChangedEvent.newBuilder()
                .setId(cmd.getUuid().toString())
                .setValue(cmd.getValue())
                .build());
    }

    @CommandHandler
    public void handle(ChangeSwedishCaptionValueCommand cmd) {
        String swedishValue = values.get(Language.SV);

        if (Objects.equals(swedishValue, cmd.getValue())) {
            return;
        }

        apply(CaptionEvents.CaptionSwedishValueChangedEvent.newBuilder()
                .setId(cmd.getUuid().toString())
                .setValue(cmd.getValue())
                .build());
    }

    @CommandHandler
    public void handle(ChangeLithuanianCaptionValueCommand cmd) {
        String lithuanianValue = values.get(Language.LT);

        if (Objects.equals(lithuanianValue, cmd.getValue())) {
            return;
        }

        apply(CaptionEvents.CaptionLithuanianValueChangedEvent.newBuilder()
                .setId(cmd.getUuid().toString())
                .setValue(cmd.getValue())
                .build());
    }

    @EventSourcingHandler
    public void handle(CaptionEvents.CaptionCreatedEvent evt) {
        this.captionId = evt.getId();
    }

    @EventSourcingHandler
    public void handle(CaptionEvents.CaptionKeyChangedEvent evt) {
        this.key = evt.getKey();
    }

    @EventSourcingHandler
    public void handle(CaptionEvents.CaptionEnglishValueChangedEvent evt) {
        this.values.put(Language.EN, evt.getValue());
    }

    @EventSourcingHandler
    public void handle(CaptionEvents.CaptionSwedishValueChangedEvent evt) {
        this.values.put(Language.SV, evt.getValue());
    }

    @EventSourcingHandler
    public void handle(CaptionEvents.CaptionLithuanianValueChangedEvent evt) {
        this.values.put(Language.LT, evt.getValue());
    }
}

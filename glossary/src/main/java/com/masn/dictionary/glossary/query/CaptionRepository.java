package com.masn.dictionary.glossary.query;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class CaptionRepository {
    private final Map<String, Caption> captions = new ConcurrentHashMap<>();

    public void insert(Caption caption) {
        captions.put(caption.getId(), caption);
    }

    public List<Caption> findAll() {
        return new ArrayList<>(captions.values());
    }

    public Caption findById(String id) {
        return captions.get(id);
    }
}

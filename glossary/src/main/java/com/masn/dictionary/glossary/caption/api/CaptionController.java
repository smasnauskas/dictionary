package com.masn.dictionary.glossary.caption.api;

import com.masn.dictionary.glossary.caption.domain.commands.*;
import com.masn.dictionary.glossary.query.Caption;
import com.masn.dictionary.glossary.query.CaptionRepository;
import lombok.Data;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/api/glossary")
public class CaptionController {
    private final CommandGateway commandGateway;
    private final CaptionRepository captionRepository;

    public CaptionController(CommandGateway commandGateway, CaptionRepository captionRepository) {
        this.commandGateway = commandGateway;
        this.captionRepository = captionRepository;
    }

    @PostMapping
    public ResponseEntity<CaptionCreatedResponseDto> createCaption() throws ExecutionException, InterruptedException {
        String id = (String) commandGateway.send(new CreateCaptionCommand(UUID.randomUUID())).get();

        CaptionCreatedResponseDto dto = new CaptionCreatedResponseDto();
        dto.setId(id);

        return ResponseEntity.ok(dto);
    }

    @PutMapping("/{uuid}/en")
    public ResponseEntity<Void> changeEnglishCaptionValue(@PathVariable UUID uuid, @RequestBody CaptionValueChangeDto dto) {
        commandGateway.send(new ChangeEnglishCaptionValueCommand(uuid, dto.getValue()));
        return ResponseEntity.accepted().build();
    }

    @PutMapping("/{uuid}/sv")
    public ResponseEntity<Void> changeSwedishCaptionValue(@PathVariable UUID uuid, @RequestBody CaptionValueChangeDto dto) {
        commandGateway.send(new ChangeSwedishCaptionValueCommand(uuid, dto.getValue()));
        return ResponseEntity.accepted().build();
    }

    @PutMapping("/{uuid}/lt")
    public ResponseEntity<Void> changeLithuanianCaptionValue(@PathVariable UUID uuid, @RequestBody CaptionValueChangeDto dto) {
        commandGateway.send(new ChangeLithuanianCaptionValueCommand(uuid, dto.getValue()));
        return ResponseEntity.accepted().build();
    }

    @PutMapping("/{uuid}/key")
    public ResponseEntity<Void> changeCaptionKey(@PathVariable UUID uuid, @RequestBody CaptionKeyChangeDto dto) {
        commandGateway.send(new ChangeCaptionKeyCommand(uuid, dto.getKey()));
        return ResponseEntity.accepted().build();
    }

    @GetMapping()
    public List<Caption> getAllCaptions() {
        return captionRepository.findAll();
    }

    @Data
    private static class CaptionCreatedResponseDto {
        private String id;
    }

    @Data
    private static class CaptionValueChangeDto {
        private String value;
    }

    @Data
    private static class CaptionKeyChangeDto {
        private String key;
    }
}

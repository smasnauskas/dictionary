package com.masn.dictionary.glossary.caption.domain.commands;

import lombok.Data;
import org.axonframework.commandhandling.TargetAggregateIdentifier;

import java.util.UUID;

@Data
public class ChangeCaptionKeyCommand {
    @TargetAggregateIdentifier
    private final UUID uuid;
    private final String key;
}

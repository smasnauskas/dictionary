package com.masn.dictionary.glossary.query;

import com.masn.dictionary.glossary.events.CaptionEvents;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Component;

@Component
public class CaptionEventHandler {
    private final CaptionRepository captionRepository;

    public CaptionEventHandler(CaptionRepository captionRepository) {
        this.captionRepository = captionRepository;
    }

    @EventHandler
    public void handle(CaptionEvents.CaptionCreatedEvent evt) {
        Caption caption = new Caption();
        caption.setId(evt.getId());
        captionRepository.insert(caption);
    }

    @EventHandler
    public void handle(CaptionEvents.CaptionKeyChangedEvent evt) {
        Caption caption = captionRepository.findById(evt.getId());
        caption.setKey(evt.getKey());
        captionRepository.insert(caption);
    }

    @EventHandler
    public void handle(CaptionEvents.CaptionEnglishValueChangedEvent evt) {
        Caption caption = captionRepository.findById(evt.getId());
        caption.setEnglishValue(evt.getValue());
        captionRepository.insert(caption);
    }

    @EventHandler
    public void handle(CaptionEvents.CaptionSwedishValueChangedEvent evt) {
        Caption caption = captionRepository.findById(evt.getId());
        caption.setSwedishValue(evt.getValue());
        captionRepository.insert(caption);
    }

    @EventHandler
    public void handle(CaptionEvents.CaptionLithuanianValueChangedEvent evt) {
        Caption caption = captionRepository.findById(evt.getId());
        caption.setLithuanianValue(evt.getValue());
        captionRepository.insert(caption);
    }
}

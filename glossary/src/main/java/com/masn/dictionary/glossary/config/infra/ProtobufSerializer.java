package com.masn.dictionary.glossary.config.infra;

import com.google.protobuf.Message;
import org.axonframework.serialization.*;
import org.springframework.lang.Nullable;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Serialization mechanism for Google Protobuf. Serializes Protobuf {@link Message} class instances. Used for reading
 * and writing events. Uses a {@link MavenArtifactRevisionResolver} which appends the Maven version of this artifact to
 * events for version tracking.
 */
public class ProtobufSerializer implements Serializer {

    private final ChainingConverter converter;
    private final ClassLoader classLoader;
    private final RevisionResolver revisionResolver;
    private final List<EventAdapter> eventAdapters;

    /**
     * Creates an instance of an Axon {@link Serializer} used for Protobuf {@link Message} serialization.
     *
     * @param groupId       Maven groupId tag value of this artifact.
     * @param artifactId    Maven artifactId tag value of this artifact.
     * @param eventAdapters Event adapters for adapting specific events.
     * @throws IOException When an exception occurs reading from the maven configuration file.
     */
    public ProtobufSerializer(String groupId, String artifactId, @Nullable List<EventAdapter> eventAdapters) throws IOException {
        this.eventAdapters = eventAdapters == null ? new ArrayList<>() : eventAdapters;
        this.converter = new ChainingConverter();
        this.classLoader = getClass().getClassLoader();
        this.revisionResolver = new MavenArtifactRevisionResolver(groupId, artifactId, classLoader);
    }

    @Override
    public <T> SerializedObject<T> serialize(Object object, Class<T> expectedRepresentation) {
        Message message = adaptForSerialization(object);
        byte[] bytes = message.toByteArray();
        T converted = converter.convert(bytes, expectedRepresentation);
        return new SimpleSerializedObject<>(converted, expectedRepresentation, typeForClass(message.getClass()));
    }

    private Message adaptForSerialization(Object object) {
        return eventAdapters.stream()
                .filter(a -> a.canAdaptForStorage(object.getClass()))
                .findFirst()
                .orElse(NoOpEventAdapter.INSTANCE)
                .adaptForStorage(object);

    }

    @Override
    public <T> boolean canSerializeTo(Class<T> expectedRepresentation) {
        return converter.canConvert(byte[].class, expectedRepresentation);
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public <S, T> T deserialize(SerializedObject<S> serializedObject) {
        SerializedObject<byte[]> bytes = converter.convert(serializedObject, byte[].class);

        SerializedType type = bytes.getType();
        Class clazz;
        try {
            clazz = classLoader.loadClass(type.getName());
        } catch (ClassNotFoundException e) {
            throw new UnknownSerializedTypeException(type, e);
        }

        Method parseFrom;
        try {
            parseFrom = clazz.getMethod("parseFrom", byte[].class);
        } catch (NoSuchMethodException e) {
            throw new UnknownSerializedTypeException(type, e);
        }

        T parsed;
        try {
            parsed = (T) parseFrom.invoke(null, (Object) bytes.getData());
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new UnknownSerializedTypeException(type, e);
        }

        return (T) adaptAfterDeserialization(parsed);
    }

    private <T> Object adaptAfterDeserialization(T object) {
        return eventAdapters.stream()
                .filter(a -> a.canAdaptFromStorage(object.getClass()))
                .findFirst()
                .orElse(NoOpEventAdapter.INSTANCE)
                .adaptFromStorage((Message) object);
    }

    @Override
    public Class classForType(SerializedType type) throws UnknownSerializedTypeException {
        String name = type.getName();
        try {
            return classLoader.loadClass(name);
        } catch (ClassNotFoundException e) {
            throw new UnknownSerializedTypeException(type, e);
        }
    }

    @Override
    public SerializedType typeForClass(Class type) {
        return new SimpleSerializedType(type.getName(), revisionResolver.revisionOf(type));
    }

    @Override
    public Converter getConverter() {
        return converter;
    }

    private enum NoOpEventAdapter implements EventAdapter {
        INSTANCE;

        @Override
        public <T> boolean canAdaptForStorage(Class<T> type) {
            return false;
        }

        @Override
        public <T> boolean canAdaptFromStorage(Class<T> type) {
            return false;
        }

        @Override
        public Message adaptForStorage(Object object) {
            return (Message) object;
        }

        @Override
        public Object adaptFromStorage(Message message) {
            return message;
        }
    }
}

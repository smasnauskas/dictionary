package com.masn.dictionary.glossary.config.infra;

import com.mongodb.MongoClient;
import org.axonframework.config.EventHandlingConfiguration;
import org.axonframework.eventhandling.tokenstore.TokenStore;
import org.axonframework.eventsourcing.eventstore.EmbeddedEventStore;
import org.axonframework.eventsourcing.eventstore.EventStore;
import org.axonframework.mongo.DefaultMongoTemplate;
import org.axonframework.mongo.MongoTemplate;
import org.axonframework.mongo.eventsourcing.eventstore.MongoEventStorageEngine;
import org.axonframework.mongo.eventsourcing.eventstore.documentperevent.DocumentPerEventStorageStrategy;
import org.axonframework.mongo.eventsourcing.tokenstore.MongoTokenStore;
import org.axonframework.serialization.xml.XStreamSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.util.Assert;

import java.io.IOException;
import java.util.List;

@Configuration
@Profile("mongo")
public class CQRSInfrastructureMongoDBConfig {

    @Value("${app.mongo.host:localhost}")
    private String host;

    @Value("${app.mongo.port:27017}")
    private Integer port;

    @Value("${app.maven.groupId}")
    private String groupId;

    @Value("${app.maven.artifactId}")
    private String artifactId;

    private final List<EventAdapter> eventAdapters;

    public CQRSInfrastructureMongoDBConfig(List<EventAdapter> eventAdapters) {
        this.eventAdapters = eventAdapters;
    }

    @Bean
    public EventStore eventStore() throws IOException {
        MongoEventStorageEngine mongoEventStorageEngine = new MongoEventStorageEngine(
                new XStreamSerializer(),
                null,
                protobufSerializer(),
                mongoTemplate(),
                new DocumentPerEventStorageStrategy());

        return new EmbeddedEventStore(mongoEventStorageEngine);
    }

    private ProtobufSerializer protobufSerializer() throws IOException {
        Assert.notNull(groupId, "Property app.maven.groupId must not be null");
        Assert.notNull(artifactId, "Property app.maven.artifactId must not be null");
        return new ProtobufSerializer(groupId, artifactId, eventAdapters);
    }

    @Bean
    public TokenStore tokenStore() {
        return new MongoTokenStore(mongoTemplate(), new XStreamSerializer());
    }

    @Autowired
    public void eventHandlingConfiguration(EventHandlingConfiguration config) {
        config.usingTrackingProcessors();
    }

    @Bean
    public MongoTemplate mongoTemplate() {
        return new DefaultMongoTemplate(mongoClient());
    }

    @Bean
    public MongoClient mongoClient() {
        return new MongoClient(host, port);
    }
}

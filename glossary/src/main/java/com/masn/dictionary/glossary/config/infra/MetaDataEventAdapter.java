package com.masn.dictionary.glossary.config.infra;

import com.google.protobuf.Message;
import com.masn.dictionary.meta.EventMetadata;
import org.axonframework.messaging.MetaData;
import org.springframework.stereotype.Component;

/**
 * Adapts the Axon's {@link MetaData} class for storage in an event store.
 */
@Component
public class MetaDataEventAdapter implements EventAdapter {

    @Override
    public <T> boolean canAdaptForStorage(Class<T> type) {
        return type.isAssignableFrom(MetaData.class);
    }

    @Override
    public <T> boolean canAdaptFromStorage(Class<T> type) {
        return type.isAssignableFrom(EventMetadata.MetaData.class);
    }

    @Override
    public EventMetadata.MetaData adaptForStorage(Object object) {
        MetaData metaData = (MetaData) object;
        EventMetadata.MetaData.Builder builder = EventMetadata.MetaData.newBuilder();
        metaData.forEach((key, value) -> builder.putValues(key, value.toString()));
        return builder.build();
    }

    @Override
    public MetaData adaptFromStorage(Message message) {
        EventMetadata.MetaData storedMetaData = (EventMetadata.MetaData) message;
        return MetaData.from(storedMetaData.getValuesMap());
    }
}

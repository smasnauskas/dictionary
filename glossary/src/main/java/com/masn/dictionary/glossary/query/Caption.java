package com.masn.dictionary.glossary.query;

import lombok.Data;

@Data
public class Caption {
    private String id;
    private String key;
    private String englishValue;
    private String swedishValue;
    private String lithuanianValue;
}

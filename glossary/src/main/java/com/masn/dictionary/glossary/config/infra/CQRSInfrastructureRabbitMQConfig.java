package com.masn.dictionary.glossary.config.infra;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("ampq")
public class CQRSInfrastructureRabbitMQConfig {

    @Bean
    public Exchange captionEventsExchange() {
        return ExchangeBuilder.fanoutExchange("CaptionEvents").build();
    }

    @Bean
    public Queue captionEventsQueue() {
        return QueueBuilder.durable("CaptionEvents").build();
    }

    @Bean
    public Binding captionEventsBinding() {
        return BindingBuilder.bind(captionEventsQueue()).to(captionEventsExchange()).with("*").noargs();
    }

    @Autowired
    public void configureAmqp(AmqpAdmin amqpAdmin) {
        amqpAdmin.declareExchange(captionEventsExchange());
        amqpAdmin.declareQueue(captionEventsQueue());
        amqpAdmin.declareBinding(captionEventsBinding());
    }
}

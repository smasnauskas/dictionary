package com.masn.dictionary.api.web;

import akka.actor.ActorRef;
import com.masn.dictionary.domain.captions.Language;
import com.masn.dictionary.domain.commands.ChangeCaptionKeyCommand;
import com.masn.dictionary.domain.commands.ChangeCaptionValueCommand;
import com.masn.dictionary.domain.commands.CreateCaptionCommand;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.UUID;

@RestController
public class CaptionCommandRestController {
    private final ActorRef captionActor;

    public CaptionCommandRestController(@Qualifier("CaptionCommandHandler") ActorRef captionActor) {
        this.captionActor = captionActor;
    }

    @PostMapping("/api/caption/")
    public ResponseEntity<Void> createCaption(@RequestBody String key) {
        UUID uuid = UUID.randomUUID();

        captionActor.tell(new CreateCaptionCommand(uuid, key), ActorRef.noSender());

        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath()
                .path("/api/caption/details/{uuid}/")
                .buildAndExpand(uuid)
                .toUri();

        return ResponseEntity.accepted().header(HttpHeaders.LOCATION, location.toString()).build();
    }

    @PostMapping("/api/caption/details/change/en/{uuid}/")
    public ResponseEntity<Void> changeCaptionValue(@PathVariable UUID uuid, @RequestBody String value) {
        captionActor.tell(new ChangeCaptionValueCommand(uuid, value, Language.EN), ActorRef.noSender());
        return ResponseEntity.accepted().build();
    }

    @PostMapping("/api/caption/details/change/key/{uuid}/")
    public ResponseEntity<Void> changeCaptionKey(@PathVariable UUID uuid, @RequestBody String key) {
        captionActor.tell(new ChangeCaptionKeyCommand(uuid, key), ActorRef.noSender());
        return ResponseEntity.accepted().build();
    }
}

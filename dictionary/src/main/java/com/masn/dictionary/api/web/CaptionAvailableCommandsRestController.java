package com.masn.dictionary.api.web;

import lombok.Data;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class CaptionAvailableCommandsRestController {

    @GetMapping("/api/caption/commands/{uuid}/")
    public ResponseEntity<RelationsDto> getCaptionRelations(@PathVariable UUID uuid) {
        Link changeEnglishValueLink = linkTo(methodOn(CaptionCommandRestController.class).changeCaptionValue(uuid, null)).withRel("caption.changeEnglishValue");
        Link changeKeyLink = linkTo(methodOn(CaptionCommandRestController.class).changeCaptionKey(uuid, null)).withRel("caption.changeKey");

        Map<String, String> relations = new HashMap<>();
        relations.put(changeEnglishValueLink.getRel(), changeEnglishValueLink.getHref());
        relations.put(changeKeyLink.getRel(), changeKeyLink.getHref());

        RelationsDto dto = new RelationsDto();
        dto.setRelations(relations);

        return ResponseEntity.ok(dto);
    }

    @Data
    private static class RelationsDto {
        private Map<String, String> relations;
    }
}

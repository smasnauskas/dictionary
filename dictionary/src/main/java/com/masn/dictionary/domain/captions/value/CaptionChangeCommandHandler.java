package com.masn.dictionary.domain.captions.value;

import com.masn.dictionary.domain.commands.ChangeCaptionKeyCommand;
import com.masn.dictionary.domain.commands.ChangeCaptionValueCommand;
import com.masn.dictionary.domain.events.CaptionDomainEvent;

import java.util.List;

public interface CaptionChangeCommandHandler {
    List<CaptionDomainEvent> handle(ChangeCaptionValueCommand cmd);

    List<CaptionDomainEvent> handle(ChangeCaptionKeyCommand cmd);
}

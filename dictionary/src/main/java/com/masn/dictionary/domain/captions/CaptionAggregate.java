package com.masn.dictionary.domain.captions;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class CaptionAggregate {
    private final UUID uuid;
    private String key;
    private Map<Language, String> values;

    public CaptionAggregate(UUID uuid, String key) {
        this.uuid = uuid;
        this.key = key;
        this.values = new HashMap<>();
    }

    public void changeKey(String key) {
        this.key = key;
    }

    public void changeValue(Language language, String value) {
        values.put(language, value);
    }

    public boolean isKeyEqual(String key) {
        return this.key != null ? this.key.equalsIgnoreCase(key) : key == null;
    }
}

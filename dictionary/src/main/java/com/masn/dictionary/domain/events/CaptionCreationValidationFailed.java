package com.masn.dictionary.domain.events;

import com.masn.dictionary.domain.captions.creation.CreationError;
import com.masn.dictionary.domain.events.handlers.CaptionDomainEventHandler;
import lombok.Data;

import java.util.UUID;

@Data
public class CaptionCreationValidationFailed implements CaptionDomainEvent {
    private final UUID uuid;
    private final CreationError error;

    public CaptionCreationValidationFailed(UUID uuid, CreationError error) {
        this.uuid = uuid;
        this.error = error;
    }

    @Override
    public UUID uuid() {
        return uuid;
    }

    @Override
    public String name() {
        return "caption-creation-validation-failed";
    }

    @Override
    public void visit(CaptionDomainEventHandler handler) {
        handler.apply(this);
    }
}

package com.masn.dictionary.domain.events;

import java.util.UUID;


public interface DomainEvent {

    UUID uuid();

    String entity();

    String name();
}

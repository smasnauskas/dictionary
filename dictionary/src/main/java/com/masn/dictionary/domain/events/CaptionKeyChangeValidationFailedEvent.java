package com.masn.dictionary.domain.events;

import com.masn.dictionary.domain.events.handlers.CaptionDomainEventHandler;
import lombok.Data;

import java.util.UUID;

@Data
public class CaptionKeyChangeValidationFailedEvent implements CaptionDomainEvent {
    private final UUID uuid;
    private final String error;

    public CaptionKeyChangeValidationFailedEvent(UUID uuid, String error) {
        this.uuid = uuid;
        this.error = error;
    }

    @Override
    public void visit(CaptionDomainEventHandler handler) {
        handler.apply(this);
    }

    @Override
    public UUID uuid() {
        return uuid;
    }

    @Override
    public String name() {
        return "caption-key-change-validation-failed-event";
    }
}

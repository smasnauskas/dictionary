package com.masn.dictionary.domain.captions;

public enum Language {
    EN, SV, LT
}

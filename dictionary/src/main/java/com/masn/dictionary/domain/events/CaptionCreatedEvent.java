package com.masn.dictionary.domain.events;

import com.masn.dictionary.domain.events.handlers.CaptionDomainEventHandler;
import lombok.Data;

import java.util.UUID;

@Data
public class CaptionCreatedEvent implements CaptionDomainEvent {
    private final UUID uuid;
    private final String key;

    public CaptionCreatedEvent(UUID uuid, String key) {
        this.uuid = uuid;
        this.key = key;
    }

    @Override
    public UUID uuid() {
        return uuid;
    }

    @Override
    public String name() {
        return "caption-created-event";
    }

    @Override
    public void visit(CaptionDomainEventHandler handler) {
        handler.apply(this);
    }
}


package com.masn.dictionary.domain.commands.handlers;

import akka.persistence.AbstractPersistentActor;
import akka.persistence.SnapshotOffer;
import com.masn.dictionary.domain.captions.CaptionAggregate;
import com.masn.dictionary.domain.captions.creation.CaptionCreationCommandHandler;
import com.masn.dictionary.domain.captions.value.CaptionChangeCommandHandler;
import com.masn.dictionary.domain.commands.ChangeCaptionKeyCommand;
import com.masn.dictionary.domain.commands.ChangeCaptionValueCommand;
import com.masn.dictionary.domain.commands.CreateCaptionCommand;
import com.masn.dictionary.domain.events.CaptionCreatedEvent;
import com.masn.dictionary.domain.events.CaptionDomainEvent;
import com.masn.dictionary.domain.events.CaptionValueChangedEvent;
import com.masn.dictionary.domain.events.handlers.CaptionDomainEventHandler;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CaptionCommandActor extends AbstractPersistentActor {
    private int snapShotInterval = 1000;

    private final CaptionDomainEventHandler eventHandler;
    private final CaptionCreationCommandHandler captionCreationCommandHandler;
    private final CaptionChangeCommandHandler captionChangeCommandHandler;

    public CaptionCommandActor(CaptionDomainEventHandler eventHandler,
                               CaptionCreationCommandHandler captionCreationCommandHandler,
                               CaptionChangeCommandHandler captionChangeCommandHandler) {
        this.eventHandler = eventHandler;
        this.captionCreationCommandHandler = captionCreationCommandHandler;
        this.captionChangeCommandHandler = captionChangeCommandHandler;
    }

    @Override
    public Receive createReceiveRecover() {
        return receiveBuilder()
                .match(SnapshotOffer.class, ss -> eventHandler.receiveSnapshot((Map<UUID, CaptionAggregate>) ss.snapshot()))
                .match(CaptionCreatedEvent.class, eventHandler::apply)
                .match(CaptionValueChangedEvent.class, eventHandler::apply)
                .build();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(CreateCaptionCommand.class, cmd -> persist(captionCreationCommandHandler.handle(cmd)))
                .match(ChangeCaptionValueCommand.class, cmd -> persist(captionChangeCommandHandler.handle(cmd)))
                .match(ChangeCaptionKeyCommand.class, cmd -> persist(captionChangeCommandHandler.handle(cmd)))
                .build();
    }

    private void persist(List<CaptionDomainEvent> events) {
        persistAll(events, evt -> {
            evt.visit(eventHandler);
            getContext().getSystem().eventStream().publish(evt);
            if (lastSequenceNr() % snapShotInterval == 0 && lastSequenceNr() != 0) {
                saveSnapshot(eventHandler.createSnapshot());
            }
        });
    }

    @Override
    public String persistenceId() {
        return "caption-store-1";
    }
}

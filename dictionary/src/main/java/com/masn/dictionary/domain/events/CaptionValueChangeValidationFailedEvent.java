package com.masn.dictionary.domain.events;

import com.masn.dictionary.domain.events.handlers.CaptionDomainEventHandler;
import lombok.Data;

import java.util.UUID;

@Data
public class CaptionValueChangeValidationFailedEvent implements CaptionDomainEvent {
    private final UUID uuid;
    private final String error;

    public CaptionValueChangeValidationFailedEvent(UUID uuid, String error) {
        this.uuid = uuid;
        this.error = error;
    }

    @Override
    public UUID uuid() {
        return uuid;
    }

    @Override
    public String name() {
        return "caption-value-change-validation-failed-event";
    }

    @Override
    public void visit(CaptionDomainEventHandler handler) {
        handler.apply(this);
    }
}

package com.masn.dictionary.domain.events;

import com.masn.dictionary.domain.captions.Language;
import com.masn.dictionary.domain.events.handlers.CaptionDomainEventHandler;
import lombok.Data;

import java.util.UUID;

@Data
public class CaptionValueChangedEvent implements CaptionDomainEvent {
    private final UUID uuid;
    private final Language language;
    private final String value;

    public CaptionValueChangedEvent(UUID uuid, Language language, String value) {
        this.uuid = uuid;
        this.language = language;
        this.value = value;
    }

    @Override
    public UUID uuid() {
        return uuid;
    }

    @Override
    public String name() {
        return "caption-value-changed-event";
    }

    @Override
    public void visit(CaptionDomainEventHandler handler) {
        handler.apply(this);
    }
}

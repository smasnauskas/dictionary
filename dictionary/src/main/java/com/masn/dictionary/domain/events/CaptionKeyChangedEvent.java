package com.masn.dictionary.domain.events;

import com.masn.dictionary.domain.events.handlers.CaptionDomainEventHandler;
import lombok.Data;

import java.util.UUID;

@Data
public class CaptionKeyChangedEvent implements CaptionDomainEvent {
    private final UUID uuid;
    private final String key;

    public CaptionKeyChangedEvent(UUID uuid, String key) {
        this.uuid = uuid;
        this.key = key;
    }

    @Override
    public void visit(CaptionDomainEventHandler handler) {
        handler.apply(this);
    }

    @Override
    public UUID uuid() {
        return uuid;
    }

    @Override
    public String name() {
        return "caption-key-changed-event";
    }
}

package com.masn.dictionary.domain.events;

import com.masn.dictionary.domain.events.handlers.CaptionDomainEventHandler;

public interface CaptionDomainEvent extends DomainEvent {
    @Override
    default String entity() {
        return "CAPTION";
    }

    void visit(CaptionDomainEventHandler handler);
}

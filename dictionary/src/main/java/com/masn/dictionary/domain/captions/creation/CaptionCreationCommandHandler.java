package com.masn.dictionary.domain.captions.creation;

import com.masn.dictionary.domain.commands.CreateCaptionCommand;
import com.masn.dictionary.domain.events.CaptionDomainEvent;

import java.util.List;

public interface CaptionCreationCommandHandler {
    List<CaptionDomainEvent> handle(CreateCaptionCommand cmd);
}

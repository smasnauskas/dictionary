package com.masn.dictionary.domain.events.handlers;

import com.masn.dictionary.domain.captions.CaptionAggregate;
import com.masn.dictionary.domain.events.*;

import java.util.Map;
import java.util.UUID;

public interface CaptionDomainEventHandler {
    void apply(CaptionCreatedEvent captionCreatedEvent);

    void apply(CaptionCreationValidationFailed captionCreationValidationFailed);

    void apply(CaptionValueChangedEvent captionValueChangedEvent);

    void apply(CaptionValueChangeValidationFailedEvent captionValueChangeValidationFailedEvent);

    void apply(CaptionKeyChangeValidationFailedEvent captionKeyChangeValidationFailedEvent);

    void apply(CaptionKeyChangedEvent captionKeyChangedEvent);

    void receiveSnapshot(Map<UUID, CaptionAggregate> snapshot);

    Map<UUID, CaptionAggregate> createSnapshot();
}

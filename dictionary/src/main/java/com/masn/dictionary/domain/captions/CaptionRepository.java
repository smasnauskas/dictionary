package com.masn.dictionary.domain.captions;

import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class CaptionRepository {
    private Map<UUID, CaptionAggregate> captions;

    public CaptionRepository() {
        captions = new ConcurrentHashMap<>();
    }

    public boolean exists(UUID uuid) {
        return captions.containsKey(uuid);
    }

    public boolean exists(String key) {
        return captions.values().stream().anyMatch(c -> c.isKeyEqual(key));
    }

    public Map<UUID, CaptionAggregate> createSnapshot() {
        return new ConcurrentHashMap<>(captions);
    }

    public void receiveSnapshot(Map<UUID, CaptionAggregate> captions) {
        this.captions = captions;
    }

    public CaptionAggregate get(UUID uuid) {
        return captions.get(uuid);
    }

    public void insert(UUID uuid, CaptionAggregate captionAggregate) {
        captions.putIfAbsent(uuid, captionAggregate);
    }
}

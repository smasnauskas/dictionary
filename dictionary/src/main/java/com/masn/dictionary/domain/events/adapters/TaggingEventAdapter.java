package com.masn.dictionary.domain.events.adapters;

import akka.persistence.journal.Tagged;
import akka.persistence.journal.WriteEventAdapter;
import com.masn.dictionary.domain.events.DomainEvent;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TaggingEventAdapter implements WriteEventAdapter {
    @Override
    public String manifest(Object event) {
        return null;
    }

    @Override
    public Tagged toJournal(Object event) {
        DomainEvent domainEvent = ((DomainEvent) event);

        Set<String> tags = Stream
                .of(domainEvent.uuid().toString(), domainEvent.entity(), domainEvent.name())
                .collect(Collectors.toSet());

        return new Tagged(event, tags);
    }
}

package com.masn.dictionary.domain.captions.value;

import com.masn.dictionary.domain.captions.CaptionRepository;
import com.masn.dictionary.domain.commands.ChangeCaptionKeyCommand;
import com.masn.dictionary.domain.commands.ChangeCaptionValueCommand;
import com.masn.dictionary.domain.events.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class CaptionChangeCommandHandlerImpl implements CaptionChangeCommandHandler {

    private final CaptionRepository captionRepository;

    public CaptionChangeCommandHandlerImpl(CaptionRepository captionRepository) {
        this.captionRepository = captionRepository;
    }

    @Override
    public List<CaptionDomainEvent> handle(ChangeCaptionValueCommand cmd) {
        List<CaptionDomainEvent> events = new ArrayList<>();

        UUID uuid = cmd.getUuid();

        if (!captionRepository.exists(uuid)) {
            CaptionValueChangeValidationFailedEvent failedEvent = new CaptionValueChangeValidationFailedEvent(uuid, "UUID_NOT_FOUND");
            events.add(failedEvent);
            return events;
        }

        CaptionValueChangedEvent captionValueChangedEvent = new CaptionValueChangedEvent(uuid, cmd.getLanguage(), cmd.getValue());
        events.add(captionValueChangedEvent);
        return events;
    }

    @Override
    public List<CaptionDomainEvent> handle(ChangeCaptionKeyCommand cmd) {
        List<CaptionDomainEvent> events = new ArrayList<>();

        UUID uuid = cmd.getUuid();

        if (!captionRepository.exists(uuid)) {
            CaptionKeyChangeValidationFailedEvent failedEvent = new CaptionKeyChangeValidationFailedEvent(uuid, "UUID_NOT_FOUND");
            events.add(failedEvent);
            return events;
        }

        CaptionKeyChangedEvent captionKeyChangedEvent = new CaptionKeyChangedEvent(uuid, cmd.getKey());
        events.add(captionKeyChangedEvent);
        return events;
    }
}

package com.masn.dictionary.domain.events.handlers;

import com.masn.dictionary.domain.captions.CaptionAggregate;
import com.masn.dictionary.domain.captions.CaptionRepository;
import com.masn.dictionary.domain.events.*;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

@Component
public class CaptionDomainEventHandlerImpl implements CaptionDomainEventHandler {
    private final CaptionRepository captionRepository;

    public CaptionDomainEventHandlerImpl(CaptionRepository captionRepository) {
        this.captionRepository = captionRepository;
    }

    @Override
    public void apply(CaptionCreationValidationFailed event) {
        // doesn't mutate state
    }

    @Override
    public void apply(CaptionValueChangeValidationFailedEvent event) {
        // doesn't mutate state
    }

    @Override
    public void apply(CaptionKeyChangeValidationFailedEvent event) {
        // doesn't mutate state
    }

    @Override
    public void apply(CaptionKeyChangedEvent event) {
        accept(event.uuid(), c -> c.changeKey(event.getKey()));
    }

    @Override
    public void apply(CaptionCreatedEvent event) {
        captionRepository.insert(event.getUuid(), new CaptionAggregate(event.getUuid(), event.getKey()));
    }

    @Override
    public void apply(CaptionValueChangedEvent event) {
        accept(event.getUuid(), caption -> caption.changeValue(event.getLanguage(), event.getValue()));
    }

    @Override
    public Map<UUID, CaptionAggregate> createSnapshot() {
        return new ConcurrentHashMap<>(captionRepository.createSnapshot());
    }

    @Override
    public void receiveSnapshot(Map<UUID, CaptionAggregate> captions) {
        captionRepository.receiveSnapshot(new ConcurrentHashMap<>(captions));
    }

    private void accept(UUID uuid, Consumer<CaptionAggregate> consumer) {
        CaptionAggregate captionAggregate = captionRepository.get(uuid);
        if (captionAggregate != null) {
            consumer.accept(captionAggregate);
        }
    }
}

package com.masn.dictionary.domain.captions.creation;

public enum CreationError {
    DUPLICATE_KEY
}

package com.masn.dictionary.domain.commands;

import com.masn.dictionary.domain.captions.Language;
import lombok.Data;

import java.util.UUID;

@Data
public class ChangeCaptionValueCommand {
    private final UUID uuid;
    private final String value;
    private final Language language;
}

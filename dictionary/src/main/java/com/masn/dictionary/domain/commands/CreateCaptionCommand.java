package com.masn.dictionary.domain.commands;

import lombok.Data;

import java.util.UUID;

@Data
public class CreateCaptionCommand {
    private final UUID uuid;
    private final String key;
}

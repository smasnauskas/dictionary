package com.masn.dictionary.domain.captions.creation;

import com.masn.dictionary.domain.captions.CaptionRepository;
import com.masn.dictionary.domain.commands.CreateCaptionCommand;
import com.masn.dictionary.domain.events.CaptionCreatedEvent;
import com.masn.dictionary.domain.events.CaptionCreationValidationFailed;
import com.masn.dictionary.domain.events.CaptionDomainEvent;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class CaptionCreationCommandHandlerImpl implements CaptionCreationCommandHandler {

    private final CaptionRepository captionRepository;

    public CaptionCreationCommandHandlerImpl(CaptionRepository captionRepository) {
        this.captionRepository = captionRepository;
    }

    @Override
    public List<CaptionDomainEvent> handle(CreateCaptionCommand cmd) {
        List<CaptionDomainEvent> events = new ArrayList<>();

        if (captionRepository.exists(cmd.getUuid())) {
            System.err.println("Caption UUID already exists");
            return events;
        }

        CaptionCreationValidationFailed captionCreationValidationFailed = null;
        CaptionCreatedEvent captionCreatedEvent = null;

        if (captionRepository.exists(cmd.getKey())) {
            captionCreatedEvent = new CaptionCreatedEvent(cmd.getUuid(), null);
            captionCreationValidationFailed = new CaptionCreationValidationFailed(cmd.getUuid(), CreationError.DUPLICATE_KEY);
        } else {
            captionCreatedEvent = new CaptionCreatedEvent(cmd.getUuid(), cmd.getKey());
        }

        events.add(captionCreatedEvent);
        events.add(captionCreationValidationFailed);

        events.removeIf(Objects::isNull);

        return events;
    }

}

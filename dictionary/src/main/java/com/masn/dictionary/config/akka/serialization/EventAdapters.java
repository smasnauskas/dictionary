package com.masn.dictionary.config.akka.serialization;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;
import com.masn.dictionary.domain.captions.Language;
import com.masn.dictionary.domain.captions.creation.CreationError;
import com.masn.dictionary.domain.events.*;
import com.masn.dictionary.event.models.EventModels;

import java.util.UUID;

public enum EventAdapters implements EventStoreAdapter {
    CAPTION_CREATED_EVENT(CaptionCreatedEvent.class, EventModels.CaptionCreatedEvent.class) {
        @Override
        public byte[] serialize(Message message) {
            return message.toByteArray();
        }

        @Override
        public Message deserialize(byte[] bytes) throws InvalidProtocolBufferException {
            return EventModels.CaptionCreatedEvent.parseFrom(bytes);
        }

        @Override
        public DomainEvent adaptFromModel(Message message) {
            EventModels.CaptionCreatedEvent model = (EventModels.CaptionCreatedEvent) message;
            return new CaptionCreatedEvent(UUID.fromString(model.getUuid()), model.getKey());
        }

        @Override
        public Message adaptToModel(DomainEvent domainEvent) {
            CaptionCreatedEvent event = (CaptionCreatedEvent) domainEvent;
            return EventModels.CaptionCreatedEvent.newBuilder()
                    .setUuid(event.getUuid().toString())
                    .setKey(event.getKey() == null ? "" : event.getKey())
                    .build();
        }
    },

    CAPTION_CREATION_VALIDATION_FAILED(CaptionCreationValidationFailed.class, EventModels.CaptionCreationValidationFailedEvent.class) {
        @Override
        public byte[] serialize(Message message) {
            return message.toByteArray();
        }

        @Override
        public Message deserialize(byte[] bytes) throws InvalidProtocolBufferException {
            return EventModels.CaptionCreationValidationFailedEvent.parseFrom(bytes);
        }

        @Override
        public DomainEvent adaptFromModel(Message message) {
            EventModels.CaptionCreationValidationFailedEvent model = (EventModels.CaptionCreationValidationFailedEvent) message;
            return new CaptionCreationValidationFailed(UUID.fromString(model.getUuid()), CreationError.valueOf(model.getError()));
        }

        @Override
        public Message adaptToModel(DomainEvent domainEvent) {
            CaptionCreationValidationFailed event = (CaptionCreationValidationFailed) domainEvent;
            return EventModels.CaptionCreationValidationFailedEvent.newBuilder()
                    .setUuid(event.getUuid().toString())
                    .setError(event.getError().name())
                    .build();
        }
    },


    CAPTION_VALUE_CHANGED_EVENT(CaptionValueChangedEvent.class, EventModels.CaptionValueChangedEvent.class) {
        @Override
        public byte[] serialize(Message message) {
            return message.toByteArray();
        }

        @Override
        public Message deserialize(byte[] bytes) throws InvalidProtocolBufferException {
            return EventModels.CaptionValueChangedEvent.parseFrom(bytes);
        }

        @Override
        public DomainEvent adaptFromModel(Message message) {
            EventModels.CaptionValueChangedEvent model = (EventModels.CaptionValueChangedEvent) message;

            Language language;
            if (model.getLanguage().isEmpty()) {
                language = Language.EN;
            } else {
                language = Language.valueOf(model.getLanguage());
            }

            return new CaptionValueChangedEvent(UUID.fromString(model.getUuid()), language, model.getValue());
        }

        @Override
        public Message adaptToModel(DomainEvent domainEvent) {
            CaptionValueChangedEvent event = (CaptionValueChangedEvent) domainEvent;
            return EventModels.CaptionValueChangedEvent.newBuilder()
                    .setUuid(event.getUuid().toString())
                    .setLanguage(event.getLanguage().name())
                    .setValue(event.getValue())
                    .build();
        }
    },

    CAPTION_VALUE_CHANGED_VALIDATION_FAILED_EVENT(CaptionValueChangeValidationFailedEvent.class, EventModels.CaptionValueChangeValidationFailedEvent.class) {
        @Override
        public byte[] serialize(Message message) {
            return message.toByteArray();
        }

        @Override
        public Message deserialize(byte[] bytes) throws InvalidProtocolBufferException {
            return EventModels.CaptionValueChangeValidationFailedEvent.parseFrom(bytes);
        }

        @Override
        public DomainEvent adaptFromModel(Message message) {
            EventModels.CaptionValueChangeValidationFailedEvent model = (EventModels.CaptionValueChangeValidationFailedEvent) message;
            return new CaptionValueChangeValidationFailedEvent(UUID.fromString(model.getUuid()), model.getError());
        }

        @Override
        public Message adaptToModel(DomainEvent domainEvent) {
            CaptionValueChangeValidationFailedEvent event = (CaptionValueChangeValidationFailedEvent) domainEvent;
            return EventModels.CaptionValueChangeValidationFailedEvent.newBuilder()
                    .setUuid(event.getUuid().toString())
                    .setError(event.getError())
                    .build();
        }
    };

    private Class clazz;
    private Class modelClazz;

    EventAdapters(Class clazz, Class modelClazz) {
        this.clazz = clazz;
        this.modelClazz = modelClazz;
    }

    public Class getClazz() {
        return clazz;
    }

    public Class getModelClazz() {
        return modelClazz;
    }
}

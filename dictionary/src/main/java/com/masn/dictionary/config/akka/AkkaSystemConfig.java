package com.masn.dictionary.config.akka;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AkkaSystemConfig {

    @Bean
    public ActorSystem actorSystem(ApplicationContext applicationContext) {
        ActorSystem system = ActorSystem.create("CaptionCommandSystem");
        SpringExtension.SpringExtProvider.get(system).initialize(applicationContext);
        return system;
    }

    @Bean(name = "CaptionCommandHandler")
    public ActorRef captionCommandHandler(ActorSystem actorSystem) {
        return actorSystem.actorOf(SpringExtension
                .SpringExtProvider
                .get(actorSystem)
                .props("captionCommandActor"), "captionCommandActor");
    }
}

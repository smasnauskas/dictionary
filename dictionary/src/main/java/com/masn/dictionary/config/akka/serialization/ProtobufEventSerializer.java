package com.masn.dictionary.config.akka.serialization;

import akka.serialization.SerializerWithStringManifest;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;
import com.masn.dictionary.domain.events.DomainEvent;

import java.util.Arrays;
import java.util.Optional;

public class ProtobufEventSerializer extends SerializerWithStringManifest {

    @Override
    public int identifier() {
        return 1000;
    }

    @Override
    public String manifest(Object object) {
        Optional<EventAdapters> adapter = Arrays.stream(EventAdapters.values())
                .filter(a -> object.getClass().equals(a.getClazz()))
                .findFirst();

        if (!adapter.isPresent()) {
            throw new IllegalArgumentException("Could not serialize event, no event adapter defined for class " + object.getClass());
        }

        return adapter.get().getModelClazz().getName();
    }

    @Override
    public byte[] toBinary(Object object) {
        Optional<EventAdapters> adapter = Arrays.stream(EventAdapters.values())
                .filter(a -> object.getClass().equals(a.getClazz()))
                .findFirst();

        if (!adapter.isPresent()) {
            throw new IllegalArgumentException("Could not serialize event, no event adapter defined for class " + object.getClass());
        }

        Message message = adapter.get().adaptToModel((DomainEvent) object);
        return adapter.get().serialize(message);
    }

    @Override
    public Object fromBinary(byte[] bytes, String manifest) {
        Optional<EventAdapters> adapter = Arrays.stream(EventAdapters.values())
                .filter(a -> a.getModelClazz().getName().equals(manifest))
                .findFirst();

        if (!adapter.isPresent()) {
            throw new IllegalArgumentException("Could not deserialize event, no event adapter defined for " + manifest);
        }

        Message message;
        try {
            message = adapter.get().deserialize(bytes);
        } catch (InvalidProtocolBufferException e) {
            throw new IllegalArgumentException("Could not deserialize event", e);
        }

        return adapter.get().adaptFromModel(message);
    }

}

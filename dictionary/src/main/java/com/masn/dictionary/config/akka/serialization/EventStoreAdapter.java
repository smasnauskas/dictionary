package com.masn.dictionary.config.akka.serialization;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;
import com.masn.dictionary.domain.events.DomainEvent;

public interface EventStoreAdapter {

    /**
     * The {@link DomainEvent} class that this adapter is responsible for.
     *
     * @return the event class that this adapter is responsible for.
     */
    Class getClazz();

    /**
     * The {@link EventAdapters} class that this adapter is responsible for.
     *
     * @return the event adapter class that this adapter is responsible for.
     */
    Class getModelClazz();

    /**
     * Serializes the Protobuf {@link Message} class to a byte array to be stored in an event store.
     *
     * @param message the Protobuf message event class.
     * @return the Protobuf byte array.
     */
    byte[] serialize(Message message);

    /**
     * Protobuf bytes to be deserialized.
     *
     * @param bytes bytes from the event store.
     * @return the parsed Protobuf event.
     * @throws InvalidProtocolBufferException throws when a Protobuf exception is encountered.
     */
    Message deserialize(byte[] bytes) throws InvalidProtocolBufferException;

    /**
     * Adapts a Protobuf {@link Message} to the {@link DomainEvent} class.
     *
     * @param message the Protobuf event model.
     * @return the {@link DomainEvent} class object.
     */
    DomainEvent adaptFromModel(Message message);

    /**
     * Adapts the {@link DomainEvent} to Protobuf model.
     *
     * @param domainEvent the event to be adapter to Protobuf event model.
     * @return the adapted {@link Message} ready to be serialized.
     */
    Message adaptToModel(DomainEvent domainEvent);
}

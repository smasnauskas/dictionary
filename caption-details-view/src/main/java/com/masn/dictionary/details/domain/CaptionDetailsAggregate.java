package com.masn.dictionary.details.domain;

import java.util.Map;

public class CaptionDetailsAggregate {
    private CaptionDetailsContext captionDetailsContext;

    public CaptionDetailsAggregate(CaptionDetailsContext captionDetailsContext) {
        this.captionDetailsContext = captionDetailsContext;
    }

    public void changeValue(Language language, String value) {
        Map<Language, String> values = captionDetailsContext.getValues();
        values.put(language, value);
    }

    public void lastError(String error) {
        captionDetailsContext.setLastError(error);
    }

    public CaptionDetailsContext getCaptionDetailsContext() {
        return captionDetailsContext;
    }

    public enum Language {
        EN,
        LT,
        SV
    }
}

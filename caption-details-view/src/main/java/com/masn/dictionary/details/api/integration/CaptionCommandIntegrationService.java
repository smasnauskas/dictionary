package com.masn.dictionary.details.api.integration;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.Data;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CaptionCommandIntegrationService {

    private final RestTemplate restTemplate;

    public CaptionCommandIntegrationService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @HystrixCommand(defaultFallback = "noCommandLinks")
    public List<Link> getCaptionCommandLinks(UUID uuid) {
        ResponseEntity<Relations> relations = restTemplate.getForEntity(UriComponentsBuilder
                .fromHttpUrl("http://dictionary/api/caption/commands/{uuid}/")
                .buildAndExpand(uuid)
                .toUri(), Relations.class);

        Relations body = relations.getBody();

        if (body == null) {
            return null;
        }

        return body.getRelations().entrySet().stream()
                .map(relation -> new Link(relation.getValue(), relation.getKey()))
                .collect(Collectors.toList());
    }

    private List<Link> noCommandLinks() {
        return null;
    }

    @Data
    private static class Relations {
        private Map<String, String> relations;
    }
}

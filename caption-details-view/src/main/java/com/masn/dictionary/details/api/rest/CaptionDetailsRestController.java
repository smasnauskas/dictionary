package com.masn.dictionary.details.api.rest;

import com.masn.dictionary.details.api.integration.CaptionCommandIntegrationService;
import com.masn.dictionary.details.domain.CaptionDetailsAggregate;
import com.masn.dictionary.details.domain.CaptionDetailsContext;
import com.masn.dictionary.details.events.CaptionDetailsRepository;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class CaptionDetailsRestController {

    private final CaptionDetailsRepository captionDetailsRepository;
    private final CaptionCommandIntegrationService captionCommandIntegrationService;

    public CaptionDetailsRestController(CaptionDetailsRepository captionDetailsRepository,
                                        CaptionCommandIntegrationService captionCommandIntegrationService) {
        this.captionDetailsRepository = captionDetailsRepository;
        this.captionCommandIntegrationService = captionCommandIntegrationService;
    }

    @GetMapping("/api/caption/details/{uuid}/")
    public ResponseEntity<CaptionDetailsDto> getCaptionDetails(@PathVariable UUID uuid) {
        CaptionDetailsAggregate captionDetails = captionDetailsRepository.getCaptionDetails(uuid);

        if (captionDetails == null) {
            return ResponseEntity.notFound().build();
        }

        CaptionDetailsDto dto = new CaptionDetailsDto();

        dto.setKey(captionDetails.getCaptionDetailsContext().getKey());
        dto.setValues(mapCaptionValues(captionDetails.getCaptionDetailsContext()));
        dto.setLastError(captionDetails.getCaptionDetailsContext().getLastError());
        dto.add(linkTo(methodOn(CaptionDetailsRestController.class).getCaptionDetails(uuid)).withSelfRel());

        List<Link> captionCommandLinks = captionCommandIntegrationService.getCaptionCommandLinks(uuid);
        if (captionCommandLinks != null) {
            dto.add(captionCommandLinks);
        }

        return ResponseEntity.ok(dto);
    }

    @GetMapping("/api/caption/search/")
    public ResponseEntity<CaptionDetailsListDto> getAllCaptionDetails() {
        Set<UUID> uuids = captionDetailsRepository.getAllUuids();

        List<CaptionDetailsListDto.CaptionDetailsListItemDto> captions = new ArrayList<>();
        for (UUID uuid : uuids) {
            CaptionDetailsListDto.CaptionDetailsListItemDto caption = new CaptionDetailsListDto.CaptionDetailsListItemDto();
            CaptionDetailsAggregate aggregate = captionDetailsRepository.getCaptionDetails(uuid);
            CaptionDetailsContext context = aggregate.getCaptionDetailsContext();
            caption.setKey(context.getKey());
            caption.add(linkTo(methodOn(CaptionDetailsRestController.class).getCaptionDetails(uuid)).withRel("caption.details"));

            List<Link> captionCommandLinks = captionCommandIntegrationService.getCaptionCommandLinks(uuid);
            if (captionCommandLinks != null) {
                caption.add(captionCommandLinks);
            }

            captions.add(caption);
        }

        CaptionDetailsListDto dto = new CaptionDetailsListDto();
        dto.setCaptions(captions);
        dto.add(linkTo(methodOn(CaptionDetailsRestController.class).getAllCaptionDetails()).withSelfRel());

        return ResponseEntity.ok(dto);
    }

    private Map<String, String> mapCaptionValues(CaptionDetailsContext context) {
        return context.getValues().entrySet().stream()
                .map(entry -> new AbstractMap.SimpleEntry<>(entry.getKey().name(), entry.getValue()))
                .collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    private static class CaptionDetailsDto extends ResourceSupport {
        private String key;
        private Map<String, String> values;
        private String lastError;
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    private static class CaptionDetailsListDto extends ResourceSupport {
        private List<CaptionDetailsListItemDto> captions;

        @EqualsAndHashCode(callSuper = true)
        @Data
        private static class CaptionDetailsListItemDto extends ResourceSupport {
            private String key;
        }
    }
}

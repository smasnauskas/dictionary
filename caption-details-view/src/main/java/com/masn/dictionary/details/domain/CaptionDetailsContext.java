package com.masn.dictionary.details.domain;

import lombok.Data;

import java.util.EnumMap;
import java.util.Map;

@Data
public class CaptionDetailsContext {
    private String key;
    private Map<CaptionDetailsAggregate.Language, String> values = new EnumMap<>(CaptionDetailsAggregate.Language.class);
    private String lastError;
}

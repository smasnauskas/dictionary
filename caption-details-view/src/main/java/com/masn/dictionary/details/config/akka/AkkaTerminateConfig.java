package com.masn.dictionary.details.config.akka;

import akka.actor.ActorSystem;
import akka.actor.Terminated;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import scala.concurrent.Future;

import javax.annotation.PreDestroy;

@Configuration
@Slf4j
public class AkkaTerminateConfig {

    private final ActorSystem actorSystem;

    public AkkaTerminateConfig(ActorSystem actorSystem) {
        this.actorSystem = actorSystem;
    }

    @PreDestroy
    public void terminateActorSystem() {
        log.info(String.format("Terminating Akka actor system %s", actorSystem.name()));

        Future<Terminated> terminate = actorSystem.terminate();

        while (!terminate.isCompleted()) {
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.info(String.format("Terminating Akka actor system %s...", actorSystem.name()));
        }

        log.info(String.format("Akka actor system %s terminated successfully", actorSystem.name()));
    }
}

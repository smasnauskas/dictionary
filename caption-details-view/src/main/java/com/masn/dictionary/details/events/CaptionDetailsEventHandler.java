package com.masn.dictionary.details.events;

import akka.actor.AbstractActor;
import com.masn.dictionary.event.models.EventModels;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("captionDetailsEventHandler")
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CaptionDetailsEventHandler extends AbstractActor {

    private final CaptionDetailsRepository captionDetailsRepository;

    public CaptionDetailsEventHandler(CaptionDetailsRepository captionDetailsRepository) {
        this.captionDetailsRepository = captionDetailsRepository;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(EventModels.CaptionCreatedEvent.class, captionDetailsRepository::apply)
                .match(EventModels.CaptionCreationValidationFailedEvent.class, captionDetailsRepository::apply)
                .match(EventModels.CaptionValueChangedEvent.class, captionDetailsRepository::apply)
                .match(EventModels.CaptionValueChangeValidationFailedEvent.class, captionDetailsRepository::apply)
                .build();
    }
}

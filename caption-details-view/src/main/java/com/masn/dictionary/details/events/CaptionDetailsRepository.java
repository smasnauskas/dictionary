package com.masn.dictionary.details.events;

import com.masn.dictionary.details.domain.CaptionDetailsAggregate;
import com.masn.dictionary.details.domain.CaptionDetailsContext;
import com.masn.dictionary.event.models.EventModels;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

@Repository
@Slf4j
public class CaptionDetailsRepository {

    private final Map<UUID, CaptionDetailsAggregate> aggregates = new ConcurrentHashMap<>();

    public CaptionDetailsAggregate getCaptionDetails(UUID uuid) {
        return aggregates.get(uuid);
    }

    public Set<UUID> getAllUuids() {
        return aggregates.keySet();
    }

    public void apply(EventModels.CaptionCreatedEvent evt) {
        CaptionDetailsContext captionDetailsContext = new CaptionDetailsContext();
        captionDetailsContext.setKey(evt.getKey());
        aggregates.putIfAbsent(UUID.fromString(evt.getUuid()), new CaptionDetailsAggregate(captionDetailsContext));
    }

    public void apply(EventModels.CaptionValueChangedEvent evt) {
        applyFor(evt.getUuid(), c -> c.changeValue(CaptionDetailsAggregate.Language.valueOf(evt.getLanguage()), evt.getValue()));
    }

    public void apply(EventModels.CaptionCreationValidationFailedEvent evt) {
        applyFor(evt.getUuid(), c -> c.lastError(evt.getError()));
    }

    public void apply(EventModels.CaptionValueChangeValidationFailedEvent evt) {
        applyFor(evt.getUuid(), c -> c.lastError(evt.getError()));
    }

    private void applyFor(String uuid, Consumer<CaptionDetailsAggregate> consumer) {
        CaptionDetailsAggregate caption = aggregates.get(UUID.fromString(uuid));

        if (caption == null) {
            log.error(String.format("No caption with UUID %s found!", uuid));
            return;
        }

        consumer.accept(caption);
    }
}

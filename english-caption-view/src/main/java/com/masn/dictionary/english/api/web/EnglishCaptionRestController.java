package com.masn.dictionary.english.api.web;

import com.masn.dictionary.english.events.handlers.EnglishCaptionEventHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class EnglishCaptionRestController {

    private final EnglishCaptionEventHandler englishCaptionEventHandler;

    public EnglishCaptionRestController(EnglishCaptionEventHandler englishCaptionEventHandler) {
        this.englishCaptionEventHandler = englishCaptionEventHandler;
    }

    @GetMapping("/api/caption/en/")
    public Map<String, String> getAllCaptions() {
        return englishCaptionEventHandler.getAllCaptions();
    }
}

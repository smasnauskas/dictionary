package com.masn.dictionary.english.config;

import akka.actor.ActorSystem;
import akka.persistence.cassandra.query.javadsl.CassandraReadJournal;
import akka.persistence.query.PersistenceQuery;
import akka.stream.ActorMaterializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AkkaJournalConfig {

    @Value("${akka.query.journal}")
    private String CASSANDRA_QUERY_JOURNAL_EXTENDED;

    @Bean
    public ActorSystem actorSystem(ApplicationContext applicationContext) {
        ActorSystem actorSystem = ActorSystem.create("EnglishCaptionView");
        SpringExtension.SpringExtProvider.get(actorSystem).initialize(applicationContext);
        return actorSystem;
    }

    @Bean
    public ActorMaterializer actorMaterializer(ActorSystem actorSystem) {
        return ActorMaterializer.create(actorSystem);
    }

    @Bean
    public CassandraReadJournal cassandraReadJournal(ActorSystem actorSystem) {
        return PersistenceQuery
                .get(actorSystem)
                .getReadJournalFor(CassandraReadJournal.class, CASSANDRA_QUERY_JOURNAL_EXTENDED);
    }
}

package com.masn.dictionary.english.events.handlers;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.persistence.cassandra.query.javadsl.CassandraReadJournal;
import akka.persistence.query.EventEnvelope;
import akka.persistence.query.Offset;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Source;
import com.masn.dictionary.english.config.SpringExtension;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class CaptionEventJournalReader implements ApplicationListener<ApplicationReadyEvent> {

    private final ActorSystem actorSystem;
    private final ActorMaterializer actorMaterializer;
    private final CassandraReadJournal cassandraReadJournal;

    public CaptionEventJournalReader(ActorSystem actorSystem, ActorMaterializer actorMaterializer, CassandraReadJournal cassandraReadJournal) {
        this.actorSystem = actorSystem;
        this.actorMaterializer = actorMaterializer;
        this.cassandraReadJournal = cassandraReadJournal;
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        ActorRef actorRef = actorSystem.actorOf(SpringExtension
                .SpringExtProvider
                .get(actorSystem)
                .props("englishCaptionActor"), "englishCaptionActor");

        Source<EventEnvelope, NotUsed> events = cassandraReadJournal.eventsByTag("CAPTION", Offset.noOffset());
        events.runForeach(evt -> actorRef.tell(evt.event(), ActorRef.noSender()), actorMaterializer);
    }
}

package com.masn.dictionary.english.events.handlers;

import com.masn.dictionary.event.models.EventModels;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Repository
@Slf4j
public class EnglishCaptionEventHandler {

    private final Map<String, String> englishCaptions = new ConcurrentHashMap<>();
    private final Map<UUID, String> uuidToKey = new ConcurrentHashMap<>();


    public void apply(EventModels.CaptionCreatedEvent evt) {
        uuidToKey.put(UUID.fromString(evt.getUuid()), evt.getKey());
        englishCaptions.put(evt.getKey(), "EMPTY");
    }

    public void apply(EventModels.CaptionValueChangedEvent evt) {
        if (!evt.getLanguage().equals("EN")) {
            return;
        }
        String key = uuidToKey.get(UUID.fromString(evt.getUuid()));

        log.debug(String.format("Old value: %s | New value: %s", englishCaptions.get(key), evt.getValue()));

        englishCaptions.replace(key, evt.getValue());
    }

    public Map<String, String> getAllCaptions() {
        return englishCaptions;
    }
}

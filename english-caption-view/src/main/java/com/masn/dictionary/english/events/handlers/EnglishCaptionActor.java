package com.masn.dictionary.english.events.handlers;

import akka.actor.AbstractActor;
import com.masn.dictionary.event.models.EventModels;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("englishCaptionActor")
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class EnglishCaptionActor extends AbstractActor {

    private final EnglishCaptionEventHandler englishCaptionEventHandler;

    public EnglishCaptionActor(EnglishCaptionEventHandler englishCaptionEventHandler) {
        this.englishCaptionEventHandler = englishCaptionEventHandler;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(EventModels.CaptionCreatedEvent.class, englishCaptionEventHandler::apply)
                .match(EventModels.CaptionValueChangedEvent.class, englishCaptionEventHandler::apply)
                .build();
    }
}
